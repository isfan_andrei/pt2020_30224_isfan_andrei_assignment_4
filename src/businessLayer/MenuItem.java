package businessLayer;

public abstract class MenuItem {
	public abstract double computePrice();
	public abstract String getNume();
}
