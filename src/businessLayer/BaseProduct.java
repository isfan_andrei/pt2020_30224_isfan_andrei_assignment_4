package businessLayer;

import java.io.Serializable;

public class BaseProduct extends MenuItem implements Serializable{
	private String nume;
	private double pret;
	public BaseProduct(String nume, double pret) {
		super();
		this.nume = nume;
		this.pret = pret;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public double getPret() {
		return pret;
	}
	public void setPret(double pret) {
		this.pret = pret;
	}
	@Override
	public double computePrice() {
		// TODO Auto-generated method stub
		return pret;
	}
	@Override
	public String toString() {
		return "" + nume;
	}
	
}
