package businessLayer;

import java.io.Serializable;
import java.util.*;

public class Order implements Serializable{
	private int orderID;
	private Date data;
	private int masa;
	
	public Order(int orderID, Date data, int masa) {
		super();
		this.orderID = orderID;
		this.data = data;
		this.masa = masa;
	}
	public int getOrderID() {
		return orderID;
	}
	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	
	public int getMasa() {
		return masa;
	}
	public void setMasa(int masa) {
		this.masa = masa;
	}
	@Override
	public String toString() {
		return "Order orderID=" + orderID + ", data=" + data + ", comanda=";
	}
	
	
}
