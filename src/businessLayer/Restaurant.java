package businessLayer;

import java.io.IOException;
import java.util.*;

import dataLayer.FileWrite;
import dataLayer.Serialization;

public class Restaurant extends Observable implements IRestaurantProcessing {
	private Serialization serialization = new Serialization();
	private ArrayList<MenuItem> produse = serialization.deserializationMenuItem();
	private HashMap<Order, ArrayList<MenuItem>> mese = serialization.deserializationOrders();
	private FileWrite fw = new FileWrite();

	public boolean esteCorect() {
		if (this instanceof Restaurant)
			return true;
		return false;
	}

	@Override
	public void createNewMenuItem(MenuItem newItem) {
		// TODO Auto-generated method stub
		assert newItem != null;
		assert esteCorect();
		try {
			produse.add(newItem);
			serialization.serializationMenuItem(produse);
		} catch (Exception ex) {
			System.out.println("Nu s-a putut realiza introducerea unui nou produs in meniu");
		}
		assert esteCorect();
		assert newItem != null;
	}

	@Override
	public void deleteMenuItem(String deletedItem) {
		// TODO Auto-generated method stub
		assert deletedItem != "";
		assert esteCorect();
		try {
			for (int i = 0; i < produse.size(); i++) {
				if (produse.get(i) instanceof BaseProduct) {
					if (produse.get(i).getNume().compareTo(deletedItem) == 0)
						produse.remove(i);
				}
				if (produse.get(i) instanceof CompositeProduct) {
					if (produse.get(i).getNume().compareTo(deletedItem) == 0)
						produse.remove(i);
				}
			}
			this.serialization.serializationMenuItem(produse);
		} catch (Exception ex) {
			System.out.println("Nu s-a putut realiza stergerea unui produs din meniu");
		}
		assert esteCorect();
		assert deletedItem != "";
	}

	@Override
	public void editMenuItem(String nume, double pretNou) {
		// TODO Auto-generated method stub
		assert nume != "";
		assert esteCorect();
		try {
			for (int i = 0; i < produse.size(); i++) {
				if (produse.get(i) instanceof BaseProduct) {
					if (produse.get(i).getNume().compareTo(nume) == 0)
						((BaseProduct) produse.get(i)).setPret(pretNou);
				}
				if (produse.get(i) instanceof CompositeProduct) {
					if (produse.get(i).getNume().compareTo(nume) == 0)
						((CompositeProduct) produse.get(i)).setPret(pretNou);
				}
			}
			this.serialization.serializationMenuItem(produse);
		} catch (Exception ex) {
			System.out.println("Nu s-a putut realiza editarea unui produs din meniu");
		}
		assert esteCorect();
		assert nume != "";
	}

	@Override
	public void createNewOrder(Order newOrder, ArrayList<MenuItem> lista) {
		// TODO Auto-generated method stub
		assert newOrder != null;
		assert esteCorect();
		try {
			this.mese.put(newOrder, lista);
			serialization.serializationOrders(mese);
		}catch(Exception ex) {
			System.out.println("Nu s-a putut realiza introducerea unei comenzi\n");
		}
		assert esteCorect();
		assert newOrder != null;
	}

	@Override
	public double computePriceForAnOrder(Order computedOrder) {
		// TODO Auto-generated method stub
		double sum = 0;
		int ji = 0;
		for (Map.Entry<Order, ArrayList<MenuItem>> i : this.getSerialization().deserializationOrders().entrySet()) {
			if(i.getKey().getOrderID() == computedOrder.getOrderID()) {
				for(int j = 0; j < i.getValue().size(); j++) {
					sum += i.getValue().get(j).computePrice();
				}
			}
		}
		return sum;
	}

	@Override
	public void generateBill(Order order) {
		// TODO Auto-generated method stub
		try {
			for (Map.Entry<Order, ArrayList<MenuItem>> i : this.getSerialization().deserializationOrders().entrySet()) {
				if(i.getKey().getOrderID() == order.getOrderID()) {
					fw.creareBon(order, this.computePriceForAnOrder(order), i.getValue());
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Eroare la crearea chitantei");
		}
	}

	public HashMap<Order, ArrayList<MenuItem>> getMese() {
		return mese;
	}

	public ArrayList<MenuItem> getProduse() {
		return produse;
	}

	public void setMese(HashMap<Order, ArrayList<MenuItem>> mese) {
		this.mese = mese;
	}

	public Serialization getSerialization() {
		return serialization;
	}

}
