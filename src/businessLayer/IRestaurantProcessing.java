package businessLayer;

import java.util.ArrayList;

public interface IRestaurantProcessing {
	public void createNewMenuItem(MenuItem newItem);
	public void deleteMenuItem(String deletedItem);
	public void editMenuItem(String nume, double pretNou);
	public void createNewOrder(Order newOrder, ArrayList<MenuItem> lista);
	public double computePriceForAnOrder(Order computedOrder);
	public void generateBill(Order order);
}
