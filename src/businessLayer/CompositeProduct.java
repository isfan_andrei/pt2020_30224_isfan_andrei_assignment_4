package businessLayer;

import java.io.Serializable;
import java.util.*;

public class CompositeProduct extends MenuItem implements Serializable{
	private String nume;
	private double pret;
	private ArrayList<MenuItem> produse = new ArrayList<MenuItem>();
	
	public CompositeProduct(String nume, double pret) {
		super();
		this.nume = nume;
		this.pret = pret;
	}

	public void adaugaProdus(MenuItem i) {
		this.produse.add(i);
	}
	
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public double getPret() {
		return pret;
	}
	public void setPret(double pret) {
		this.pret = pret;
	}

	public ArrayList<MenuItem> getProduse() {
		return produse;
	}

	@Override
	public double computePrice() {
		// TODO Auto-generated method stub
		return pret;
	}

	@Override
	public String toString() {
		return "" + nume;
	}
	
	
}
