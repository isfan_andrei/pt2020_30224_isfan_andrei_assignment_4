package dataLayer;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import businessLayer.MenuItem;
import businessLayer.Order;

public class Serialization {
	public void serializationMenuItem(ArrayList<MenuItem> item) {
		try {
			FileOutputStream fileOut = new FileOutputStream("Restaurant.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(item);
			out.close();
			fileOut.close();
			System.out.printf("Serialized data is saved in Restaurant.ser\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<MenuItem> deserializationMenuItem() {

		ArrayList<MenuItem> items = new ArrayList<MenuItem>();
		try {
			FileInputStream fileIn = new FileInputStream("Restaurant.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			items = (ArrayList<MenuItem>) in.readObject();
			in.close();
			fileIn.close();
			return items;
		} catch (IOException e) {
			//e.printStackTrace();
			return items;
		} catch (ClassNotFoundException e) {
			System.out.println("Eroare la deserializare");
			e.printStackTrace();
			return items;
		}
	}
	
	public void serializationOrders(HashMap<Order, ArrayList<MenuItem>> mese) {
		try {
			FileOutputStream fileOut = new FileOutputStream("Orders.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(mese);
			out.close();
			fileOut.close();
			System.out.printf("Serialized data is saved in Restaurant.ser\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public HashMap<Order, ArrayList<MenuItem>> deserializationOrders() {

		HashMap<Order, ArrayList<MenuItem>> orders = new HashMap<Order, ArrayList<MenuItem>>();
		try {
			FileInputStream fileIn = new FileInputStream("Orders.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			orders = (HashMap<Order, ArrayList<MenuItem>>) in.readObject();
			in.close();
			fileIn.close();
			return orders;
		} catch (IOException e) {
			//e.printStackTrace();
			return orders;
		} catch (ClassNotFoundException e) {
			System.out.println("Eroare la deserializare");
			e.printStackTrace();
			return orders;
		}
	}
}
