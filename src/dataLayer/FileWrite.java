package dataLayer;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.awt.CardLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.IntrospectionException;
import java.io.*;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import businessLayer.MenuItem;
import businessLayer.Order;

public class FileWrite {
	
	public static int numarChitanta = 1;

	public void creareBon(Order order, double total, ArrayList<MenuItem> mese) throws IOException {
		String locatie = "C:\\Users\\andre\\eclipse-workspace\\Proiect4\\Chitanta" + numarChitanta + ".txt";
		ArrayList<String> str = new ArrayList<>();
		str.add("Nota de plata numarul " + numarChitanta + "\n");
		for (int i = 0; i < mese.size(); i++) {
			str.add(mese.get(i).toString() + "  " + mese.get(i).computePrice() + "lei\n");
		}
		str.add("Total: " + total + "lei\n");
		str.add("Masa:" + order.getMasa() + "\n");
		str.add("Data: " + order.getData().toString() + "\n");
		str.add("Va mai asteptam!\n");

		try (FileWriter writer = new FileWriter(locatie); BufferedWriter bw = new BufferedWriter(writer)) {

			for (String string : str) {
				bw.write(string);
				bw.newLine();
			}

			ProcessBuilder pb = new ProcessBuilder("Notepad.exe", locatie);
			pb.start();

			numarChitanta++;
		} catch (IOException e) {
			//e.printStackTrace();
		}
	}

}
