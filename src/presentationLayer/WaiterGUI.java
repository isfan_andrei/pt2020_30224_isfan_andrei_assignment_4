package presentationLayer;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import javax.swing.*;

import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;

public class WaiterGUI extends JFrame implements ActionListener {
	public JFrame frame = new JFrame("Ospatar");
	public JPanel panel = new JPanel();
	public JButton butonNewOrder = new JButton("Creare comanda noua");
	public JButton butonCompute = new JButton("Finalizare comanda");
	public JButton butonViewAllOrder = new JButton("Vizualizare comenzi");
	public JButton addButton = new JButton("+");
	public JButton clearButton = new JButton("sterge");
	public JButton inapoi = new JButton("Inapoi");
	public JComboBox combo1 = new JComboBox();
	public static JComboBox combo2 = new JComboBox();
	public JTextField text = new JTextField();
	public JComboBox mese = new JComboBox();
	public JFrame frame2 = new JFrame("Lista comenzi");
	public JPanel panel2 = new JPanel();

	public Restaurant restaurant = new Restaurant();
	public ArrayList<MenuItem> produse = new ArrayList<MenuItem>();
	public static int index = 1;

	public WaiterGUI() {
		index = restaurant.getSerialization().deserializationOrders().size();
		JPanel auxP1 = new JPanel();
		JPanel auxP2 = new JPanel();
		JPanel auxP3 = new JPanel();
		JPanel auxP4 = new JPanel();
		JPanel panelAUX = new JPanel();
		butonNewOrder.addActionListener(this);
		butonViewAllOrder.addActionListener(this);
		butonCompute.addActionListener(this);
		addButton.addActionListener(this);
		clearButton.addActionListener(this);
		inapoi.addActionListener(this);
		text.setEditable(false);
		auxP1.add(butonNewOrder);
		auxP1.add(butonViewAllOrder);
		auxP1.add(butonCompute);
		auxP1.add(inapoi);
		auxP2.add(combo1);
		auxP2.add(combo2);
		auxP3.add(addButton);
		auxP3.add(mese);
		mese.addItem("masa");
		for (int i = 1; i <= 10; i++)
			mese.addItem(i);
		auxP3.add(clearButton);
		auxP1.setLayout(new BoxLayout(auxP1, BoxLayout.Y_AXIS));
		auxP2.setLayout(new BoxLayout(auxP2, BoxLayout.Y_AXIS));
		this.formareCombo1();
		panelAUX.add(auxP1);
		panelAUX.add(auxP2);
		panelAUX.add(auxP3);
		panelAUX.add(auxP4);
		panel.add(panelAUX);
		panel.add(text);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		frame.add(panel);
		frame.setSize(550, 200);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame2.setSize(500, 400);
		frame2.setDefaultCloseOperation(2);
		frame2.setVisible(false);
	}

	public void veziComenzi() {
		String data[][] = new String[restaurant.getSerialization().deserializationOrders().size()][4];
		int iii = 0;
		for (Map.Entry<Order, ArrayList<MenuItem>> i : restaurant.getSerialization().deserializationOrders()
				.entrySet()) {
			data[iii][0] = "" + i.getKey().getOrderID();
			data[iii][1] = "" + i.getKey().getData();
			data[iii][3] = "" + i.getKey().getMasa();
			String str = "";
			for (MenuItem j : i.getValue()) {
				str += j.getNume() + ", ";
			}
			str = str.substring(0, str.length() - 2);
			data[iii][2] = "" + str;
			iii++;
		}
		String column[] = { "Id", "Data", "Comanda", "Masa" };
		JTable jt = new JTable(data, column);
		jt.setBounds(100, 100, 100, 100);
		JScrollPane sp = new JScrollPane(jt);
		panel2.removeAll();
		panel2.add(sp);
		frame2.add(panel2);
		frame2.setVisible(true);
	}

	public void formareCombo1() {
		for (int i = 0; i < this.restaurant.getSerialization().deserializationMenuItem().size(); i++) {
			this.combo1.addItem(this.restaurant.getSerialization().deserializationMenuItem().get(i).getNume());
		}
	}

	public void adaugaProdusPeLista() {
		for (int i = 0; i < this.restaurant.getSerialization().deserializationMenuItem().size(); i++) {
			if (this.restaurant.getSerialization().deserializationMenuItem().get(i).getNume()
					.compareTo(this.combo1.getSelectedItem().toString()) == 0) {
				this.produse.add(this.restaurant.getSerialization().deserializationMenuItem().get(i));
				if (this.text.getText().isEmpty() == false)
					this.text.setText(this.text.getText() + ", " + this.combo1.getSelectedItem().toString());
				else
					this.text.setText(this.text.getText() + "" + this.combo1.getSelectedItem().toString());
			}
		}
	}

	void newOrder() {
		if (text.getText().compareTo("") != 0) {
			ArrayList<MenuItem> auxMenuItems = new ArrayList<MenuItem>();
			for (MenuItem i : this.produse) {
				auxMenuItems.add(i);
			}
			try {
				int x = Integer.parseInt(mese.getSelectedItem().toString());
				this.restaurant.createNewOrder(new Order(index, new Date(), Integer.parseInt(mese.getSelectedItem().toString())),auxMenuItems);
			} catch (IllegalArgumentException e) {
				JPanel newPanel = new JPanel();
				JTextField newText = new JTextField("Va rugam sa introduceti numarul mesei :)");
				JFrame newFrame = new JFrame();
				newPanel.add(newText);
				newFrame.add(newPanel);
				newFrame.setSize(100, 40);
				newFrame.setDefaultCloseOperation(8);
				newFrame.setVisible(true);
			}
			this.combo2.addItem(index);
			index++;
			this.text.setText("");
			this.produse.clear();
		}
	}

	public void creareNotaDePlata() {
		int aux = Integer.parseInt(combo2.getSelectedItem().toString());
		combo2.removeItem(aux);
		for (Map.Entry<Order, ArrayList<MenuItem>> order : restaurant.getSerialization().deserializationOrders()
				.entrySet()) {
			if (order.getKey().getOrderID() == aux) {
				restaurant.generateBill(order.getKey());
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == inapoi) {
			frame.setVisible(false);
			FereastraPrincipala start = new FereastraPrincipala();
		}
		if (e.getSource() == butonNewOrder) {
			this.newOrder();
		}
		if (e.getSource() == butonViewAllOrder) {
			this.veziComenzi();
		}
		if (e.getSource() == butonCompute) {
			this.creareNotaDePlata();
		}
		if (e.getSource() == addButton) {
			this.adaugaProdusPeLista();
		}
		if (e.getSource() == clearButton) {
			this.text.setText("");
			this.produse.clear();
		}
	}
}
