package presentationLayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import javax.swing.*;

import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;

public class ChefGUI implements Observer {

	public JFrame frame = new JFrame("Bucatar");
	public JPanel panel = new JPanel();
	public JButton inapoi = new JButton("Inapoi");
	public JTextArea text = new JTextArea("Comenzi:\n");

	public ChefGUI() {
		inapoi.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FereastraPrincipala back = new FereastraPrincipala();
				frame.setVisible(false);
			}
		});
		panel.add(text);
		update(new Observable(), new Object());
		panel.add(inapoi);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		frame.add(panel);
		frame.setSize(400, 300);
		frame.setDefaultCloseOperation(2);
		frame.setVisible(true);
	}

	public Restaurant restaurant = new Restaurant();
	public int index = 1;

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		for (Map.Entry<Order, ArrayList<MenuItem>> i : restaurant.getSerialization().deserializationOrders()
				.entrySet()) {
			text.setText(text.getText() + index + "." + i.getValue().toString() + " - " + i.getKey().getData() + "\n");
			index++;
		}
	}
}
