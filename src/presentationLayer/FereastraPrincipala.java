package presentationLayer;

import java.awt.event.*;

import javax.swing.*;

public class FereastraPrincipala extends JFrame{

	public JFrame frame = new JFrame("Restaurant");
	public JPanel panelPrincipal = new JPanel();
	public JPanel panel1 = new JPanel();
	public JPanel panel2 = new JPanel();
	public JLabel label = new JLabel("Selectati utilizatorul");
	public JButton buton1 = new JButton("Administrator");
	public JButton buton2 = new JButton("Ospatar");
	public JButton buton3 = new JButton("Bucatar");
	
	public FereastraPrincipala() {
		buton1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AdministratorGUI admin = new AdministratorGUI();
				frame.setVisible(false);
			}
		});
		buton2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				WaiterGUI chelner = new WaiterGUI();
				frame.setVisible(false);
			}
		});
		buton3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ChefGUI bucatar = new ChefGUI();
				frame.setVisible(false);
			}
		});
		panel1.add(label);panel2.add(buton1);panel2.add(buton2);panel2.add(buton3);
		panelPrincipal.add(panel1);panelPrincipal.add(panel2);
		panelPrincipal.setLayout(new BoxLayout(panelPrincipal, BoxLayout.Y_AXIS));
		frame.add(panelPrincipal);
		frame.setSize(400, 160);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
